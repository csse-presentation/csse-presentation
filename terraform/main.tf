terraform {
  backend "http" {}
}

provider "aws" {
  region = "us-west-1"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "demo-vpc"
  cidr = "192.168.0.0/16"

  azs             = ["us-west-1a", "us-west-1c"]
  private_subnets = ["192.168.1.0/24", "192.168.3.0/24"]
  public_subnets  = ["192.168.4.0/24", "192.168.6.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  single_nat_gateway = true
  default_vpc_enable_dns_hostnames = true
  enable_dns_hostnames = true
}

module "ecs" {
  source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/compute/ecs-fargate-with-efs"

  cluster-name = "demo-cluster"
  container-image = "registry.gitlab.com/csse-presentation/csse-presentation"
  container-tag = "latest"
  lb-health-check-url = "/test"

  ecs_subnets = module.vpc.private_subnets
  lb-subnets = module.vpc.public_subnets
  vpc_id = module.vpc.vpc_id

  fargate-spot-percentage = 100
  base-on-demand-container-count = 0

  # For now, we'll just run the demo on HTTP so we don't have to provision a certificate.
  lb-listener-port = 80
  lb-internal = false
}

# Below is for autoscaling

resource "aws_appautoscaling_policy" "scale_jira" {
  name = "demo-scaling-rule"

  policy_type        = "TargetTrackingScaling"
  resource_id        = module.ecs.ecs-autoscaling-resource-name
  scalable_dimension = module.ecs.ecs-autoscaling-dimension
  service_namespace  = module.ecs.ecs-autoscaling-namespace

  target_tracking_scaling_policy_configuration {
    target_value = "500"

    scale_in_cooldown  = "10"
    scale_out_cooldown = "10"

    predefined_metric_specification {
      predefined_metric_type = "ALBRequestCountPerTarget"
      resource_label = "${data.aws_lb.fargate-lb.arn_suffix}/${data.aws_lb_target_group.fargate-tg.arn_suffix}"
    }
  }
}

data "aws_lb" "fargate-lb" {
  arn = module.ecs.load-balancer-arn
}

data "aws_lb_target_group" "fargate-tg" {
  arn = module.ecs.load-balancer-tg-arn
}